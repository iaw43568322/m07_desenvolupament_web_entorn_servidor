var path = require("path"),
  express = require("express"),
  router = express.Router(),
  ctrlDir = "/app/app/controllers",
  carsCtrl = require(path.join(ctrlDir, "cars")),
  rentsCtrl = require(path.join(ctrlDir, "rents")),
  clientsCtrl = require(path.join(ctrlDir, "clients"));

/* CARS API */
// var ctrlCars = require(path.join(ctrlDir, "cars"));

router.get("/cars", carsCtrl.getAll());

/* router.route("/cars").get(async(req, res, next) => {
  var res = await ctrlCars.getAll();
  console.log(res);
}); */

/* CLIENTS API */
// var ctrlClients = require(path.join(ctrlDir, "clients"));

/* RENTS API */
// var ctrlRents = require(path.join(ctrlDir, "rents"));

module.exports = router;
