var mongoose = require("mongoose");
Schema = mongoose.Schema;

// Creem la estructura
var rentSchema = new Schema({
    id: {type: String},
    brand: {type: String},
    model: {type: String},
    price: {type: String},
});

module.exports = mongoose.model("rent", rentSchema);