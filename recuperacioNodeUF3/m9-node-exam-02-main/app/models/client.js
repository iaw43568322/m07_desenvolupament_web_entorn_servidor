var mongoose = require("mongoose");
Schema = mongoose.Schema;

// Creem la estructura
var clientSchema = new Schema({
    id: {type: String},
    name: {type: String},
    surname: {type: String},
});

module.exports = mongoose.model("client", clientSchema);