$(document).ready(function () {
  //Inicializa socket con IO
  // const socket = io.connect('http://localhost:3000', { 'forceNew': true});
  const socket = io();

  $("#send").submit(function (e) {
    e.preventDefault();
    var msg = $("#msg").val();
    //Acciones a realizar cuando se pulsa el boton submit
    console.log(msg)
     socket.emit('default', msg);
   
  });

  //We listen on "newMsg" and react if we detect 
  //Acciones a realizar cuando se detecta actividad en el canal newMsg

});
