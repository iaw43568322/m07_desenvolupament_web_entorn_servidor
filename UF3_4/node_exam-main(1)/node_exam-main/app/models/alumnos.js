var mongoose = require("mongoose");
Schema = mongoose.Schema;

var alumnosSchema = new Schema({
    nombre: {type: String},
    apellido: {type: String}
});

module.exports = mongoose.model("alumnos", alumnosSchema);