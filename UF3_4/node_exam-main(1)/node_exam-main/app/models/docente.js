var mongoose = require("mongoose");
Schema = mongoose.Schema;

var docentesSchema = new Schema({
    nombre: {type: String},
    apellido: {type: String}
});

module.exports = mongoose.model("docentes", docentesSchema);