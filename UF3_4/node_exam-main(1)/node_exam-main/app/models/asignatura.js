var mongoose = require("mongoose");
Schema = mongoose.Schema;

var asignaturaSchema = new Schema({
    nombre: {type: String},
    numHoras: {type: String},
    docente: {type: Object},
    alumnos: {type: [Object]}
});

module.exports = mongoose.model("asignatura", asignaturaSchema);