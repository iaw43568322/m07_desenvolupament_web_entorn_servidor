var mongoose = require("mongoose"),
    Docentes = require("../models/docente");

exports.load = async() => {
    var res = await Docentes.find({});
    return res;
}