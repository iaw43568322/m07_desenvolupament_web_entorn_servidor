var mongoose = require("mongoose"),
    Asignatura = require("../models/asignatura");

// Listar asignaturas
exports.load= async(req) => {
    var res = await Asignatura.find({});
    return res;
}

// Crear nueva asignatura
exports.save=(req) => {
    var newAsignatura = new Asignatura(req);
    newAsignatura.save((err,res) => {
        if(err) {
            console.log("ERROR: " + err);
        } else {
            console.log("Nueva asignatura guardada");
        }
    });
}