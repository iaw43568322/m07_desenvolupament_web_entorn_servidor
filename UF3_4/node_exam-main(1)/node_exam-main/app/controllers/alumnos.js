var mongoose = require("mongoose"),
    Alumnos = require("../models/alumnos");

exports.load = async() => {
    var res = await Alumnos.find({});
    return res;
}