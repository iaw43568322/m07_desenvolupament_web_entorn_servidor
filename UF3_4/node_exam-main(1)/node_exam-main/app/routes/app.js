var path = require("path");
var express = require("express");
var router = express.Router();

var alumnosCntr = require(__dirname+"/../controllers/alumnos");
var docentesCntr = require(__dirname+"/../controllers/docentes");
var asignaturaCntr = require(__dirname+"/../controllers/asignaturas");

router.route("/").get(async(req,res,next) => {
    var result = await alumnosCntr.load();
    console.log(result);
    var resultado = await docentesCntr.load();
    console.log(resultado);
    res.render("chat");
});

router.route("asignatura/save").get((req,res,next) => {
    var newAsignatura = {
        nombre: "Matemáticas",
        numHoras: "120",
        docente: "Victor",
        alumnos: [
            {nombre: "Ignasi",
            apellido: "Brugada"},
            {nombre: "David",
            apellido: "Fernandez"}
        ]
    }
    asignaturaCntr.save(newAsignatura);
    res.send("Guardada con éxito");
})

module.exports = router;