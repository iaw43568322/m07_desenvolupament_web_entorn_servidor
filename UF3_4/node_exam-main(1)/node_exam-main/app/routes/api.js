var path = require("path");
var express = require("express");
var router = express.Router();

var asignaturaCntr = require(__dirname+"/../controllers/asignaturas");

router.route("/").get(async(req,res,next) => {
    var result = await asignaturaCntr.load();
    
    res.status(200).jsonp(result);
});

module.exports = router;