<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Píldora 2 exercici 2</title>
    </head>
    <body>
        <h1>Píldora 2 exercici 2</h1>
        <form method="post" action="pildora2_ex2_recibo.php">
            Introdueix dos números i selecciona una operació <br>
            <input type="text" name="num1"> <br>
            <input type="text" name="num2"> <br>
            <select name="operacio">
                <option>+</option>
                <option>-</option>
                <option>*</option>
                <option>/</option>
                <option>**</option>
            </select>
        </form>
    </body>
</html>