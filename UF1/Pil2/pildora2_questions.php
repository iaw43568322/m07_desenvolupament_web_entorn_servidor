<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Exercicis Píldora 2</title>
    </head>
    <body>
        <h1>Píldora 2: Comunicación entre páginas</h1>
        <h5>Question 1: Crea una URL en la que pasemos el nombre y la edad del usuario
        actual y posteriormente recuperalo con las superglobales $_GET</h5>
        <p>
            URL:    www.pildora2.com/exercicisPildora2.php?name=Ignasi&edad=20
            <br>
            <?php
                // Guardem el nom i l'edad que li hem dit a la URL 
                $nom = $_GET['name'];
                $edad = $_GET['edad'];
                // Mostrem les dades amb un echo
                echo "<p>Nom: " . $nom . "<br>Edad: " . $edad . "</p>";
            ?>
        </p>

        <h5>Question 2: Crea un formulario que envie a otra página los datos
        personales de una persona en formato POST. Escribe el código del script que 
        recibe esta información para mostrarla en pantalla</h5>
        <p>
            <form method="psot" action="recibo.php">
                Este es un formulario de tus datos personales <br>
                Nombre: <input type="text" name="nombre"> <br>
                Apellidos: <input type="text" name="apellidos"> <br>
                Correo electronico: <input type="text" name="correo">
            </form>
        </p>
        <p>
            <?php
                // Guardem tots els valors passats pel formulari
                $nombre = $_POST["nombre"];
                $apellidos = $_POST["apellidos"];
                $correo = $_POST["correo"];
                // Mostrem per pantalla totes les dades
                echo "Nombre: " . $nombre . "<br>Apellidos: " . $apellidos . "<br>
                    Correo electrónico: " . $correo;
            ?>
        </p>
    </body>
</html>