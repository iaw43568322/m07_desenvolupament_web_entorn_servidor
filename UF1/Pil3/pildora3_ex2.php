<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Píldora 3 exercici 2</title>
    </head>
    <body>
        <h5>
            Dado el siguiente array $loteria = array(61, 32, 43, 61). 
            Haz un programa que muestre por pantalla el numero de veces que aparece
            el numero 61
        </h5>
        <?php
            // Declaració de variables
            $loteria = array(61, 32, 43, 61);
            $numero = 61;
            $cont = 0;
            // Recorrem tot l'array i mirem quants cops apareix el $numero
            for ($i = 0; $i < count($loteria); $i++) {
                if ($loteria[$i] === $numero) {
                    $cont++;
                }
            }
            // Mostrem per pantalla quants cops apareix el $numero
            echo "El número " . $numero . " apareix " . $cont . " cops";
        ?>
    </body>
</html>