<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Píldora 3 exercici 3</title>
    </head>
    <body>
        <h5>
            Utilizando el array del ejercicio anterior, crea un programa que cuente
            el número de veces que aparece un número dado a través de un formulario post.
            En la página del resultado ha de aparecer el número que se busca
            y el número de coincidencias en una frase 'bonita'
        </h5>
        <!-- Creem el formulari on demanarem quin número vol mirar si es repeteix -->
        <form method="post" action="pildora3_ex3_recibo.php">
            Que número quiere ver cuantas veces se repite en el array?
            <input type="text" name="numero">
        </form>
    </body>
</html>