<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Píldora 3 exercici 3</title>
    </head>
    <body>
        <?php
            // Declaració de variables
            $loteria = array(61, 32, 43, 61);
            $numero = $_POST["numero"];
            $cont = 0;
            // Recorrem tot l'array i mirem quants cops apareix el $numero
            for ($i = 0; $i < count($loteria); $i++) {
                if ($loteria[$i] == $numero) {
                    $cont++;
                }
            }
            // Mostrem quin número era i quants cops apareix
            echo "El número " . $numero . " aparece " . $cont . " veces en el array";
        ?>
    </body>
</html>