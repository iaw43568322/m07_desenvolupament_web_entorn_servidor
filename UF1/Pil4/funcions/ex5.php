<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Exercici 5 pil4</title>
</head>
<body>
    <?php
        // Declaració de variables
        $num1 = $_POST["num1"];
        $num2 = $_POST["num2"];
        /**
         * Función que compara dos números y devuelve 1, -1 o 0 según el resultado
         * 
         * @param double número 1
         * @param double número 2
         * @return int 1, -1 o 0
         */
        function relacion($num1, $num2) {
            // Si el primer número es mayor que el segundo, debe devolver 1
            if ($num1 > $num2) {
                return 1;
            } // Si el primer número es menor que el segundo, debe devolver -1
            else if ($num1 < $num2) {
                return -1;
            } // Si ambos número son iguales, debe devolver 0
            else {
                return 0;
            }
        }
        // Llamamos a la función
        echo relacion($num1, $num2);
    ?>
</body>
</html>