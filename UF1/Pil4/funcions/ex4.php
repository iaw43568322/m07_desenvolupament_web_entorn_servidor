<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Exercici 4 pil4</title>
</head>
<body>
    <?php
        // Declaració de variables
        $preuCompra = $_POST["preuCompra"];
        /**
         * Funció que aplica un desconte segons el preu de la compra
         * 
         * @param double preu de la compra
         * @return double preu final
         */
        function preuCompraFinal($preuCompra) {
            // Si el preu de la compra està entre 100 i 499,99 es desconte el 10%
            if ($preuCompra >= 100 && $preuCompra < 500) {
                $preuFinal = $preuCompra - ($preuCompra * 10 / 100);
            } // Si el preu de la compra supera els 500 es desconte un 15%
            else if ($preuCompra >= 500) {
                $preuFinal = $preuCompra - ($preuCompra * 15 / 100);
            } // Si la compra no supera els 100 no hi ha desconte
            else {
                $preuFinal = $preuCompra;
            }
            return $preuFinal;
        }
        echo "El preu final de la seva compra és de ". preuCompraFinal($preuCompra);
    ?>
</body>
</html>