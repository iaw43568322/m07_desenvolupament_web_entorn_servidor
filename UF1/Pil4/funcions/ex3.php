<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Exercici 3 pil4</title>
</head>
<body>
    <?php
        // Declaració de variables
        $radi = $_POST["radi"];
        $altura = $_POST["altura"];
        $numPi = 3.1416;
        /**
         * Funció que calcula el volum d'un cilindre
         * 
         * @param double radi del cilindre
         * @param double altura del cilindre
         * @return double volum del cilindre
         */
        function calcularVolum($radi, $altura) {
            global $numPi;
            $volum = $numPi * $radi * $radi * $altura;
            return $volum;
        }
        calcularVolum($radi, $altura);
    ?>
</body>
</html>