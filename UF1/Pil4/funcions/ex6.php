<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Exercici 6 pil4</title>
</head>
<body>
    <?php
        // Declaració de variables
        $num1 = $_POST["num1"];
        $num2 = $_POST["num2"];
        /**
         * Funció que, donats dos números, retorna el seu punt intermig
         * 
         * @param double número 1
         * @param double número 2
         * @return double punt intermig entre num1 i num2
         */
        function intermedio($num1, $num2) {
            // Calculem el punt intermig entre num1 i num2
            if ($num1 > $num2) {
                $resta = $num1 - $num2;
                return $resta / 2 + $num2;
            } else {
                $resta = $num2 - $num1;
                return $resta / 2 + $num1;
            }
        }
        echo "El punt intermig entre ". $num1 . " i ". $num2 . " és ". intermedio($num1, $num2);
    ?>
</body>
</html>