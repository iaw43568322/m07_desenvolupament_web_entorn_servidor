<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Funcions PHP</title>
</head>
<body>
    <h1>Píldora 4: Programación estructurada</h1>
    <h5>
        1. Una función que reciba cinco números enteros como paràmetros y muestre
        por pantalla el resultado de sumar los cinco números
    </h5>
    <form method="post" action="ex1.php">
        Num1: <input type="text" name="num1"> <br>
        Num2: <input type="text" name="num2"> <br>
        Num3: <input type="text" name="num3"> <br>
        Num4: <input type="text" name="num4"> <br>
        Num5: <input type="text" name="num5"> <br>
        <input type="submit" value="Enviar">
    </form>
    <h5>
        2. Una función que reciba cinco números enteros como paràmetros y devuelva
        el resultado de sumar los cinco números. Asigna el resultado de una invocación
        a la función con los números 2, 5, 1, 8, 10 a una variable de nombre $tmp
        y muestra por pantalla el valor de la variable.
    </h5>
    <form method="post" action="ex2.php">
        Num1: <input type="text" name="num1"> <br>
        Num2: <input type="text" name="num2"> <br>
        Num3: <input type="text" name="num3"> <br>
        Num4: <input type="text" name="num4"> <br>
        Num5: <input type="text" name="num5"> <br>
        <input type="submit" value="Enviar">
    </form>
    <h5>
        3. Una función que reciba como parámetros el valor del radio de la base
        y la altura de un cilindro y devuelva el volumen del cilindro, teniendo
        en cuenta que el volumen de un cilindro se calcula como 
        Volumen = númeroPi * radio * radio * Altura siendo 
        númeroPi = 3.1416 aproximadamente.
    </h5>
    <form method="post" action="ex3.php">
        Radio: <input type="text" name="radi"> <br>
        Altura: <input type="text" name="altura"> <br>
        <input type="submit" value="Enviar">
    </form>
    <h5>
        4. Crear un formulario en el que se introduce el precio de una compra
        y realizar el programa que calcula mediante una función una serie de reglas
    </h5>
    <form method="post" action="ex4.php">
        Precio de compra: <input type="text" name="preuCompra"> <br>
        <input type="submit" value="Enviar">
    </form>
    <h5>
        5. Realiza una función llamada relacion(a, b) que a partir
        de dos números cumpla unos requisitos
    </h5>
    <form method="post" action="ex5.php">
        Número 1: <input type="text" name="num1"> <br>
        Número 2: <input type="text" name="num2"> <br>
        <input type="submit" value="Enviar">
    </form>
    <h5>
        6. Realiza una función llamada intermedio(a, b) que a partir de dos números,
        devuelva su punto intermedio.
        Cuando lo tengas comprueba el punto intermedio entre -12 y 24
    </h5>
    <form method="post" action="ex6.php">
        Número 1: <input type="text" name="num1"> <br>
        Número 2: <input type="text" name="num2"> <br>
        <input type="submit" value="Enviar">
    </form>
    <h5>
        7. Realiza una función separar(lista) que tome una lista de números enteros
        y devuelva (no imprimir) dos listas ordenadas. La primera con los números
        pares y la segunda con los números impares.
    </h5>
    <form method="post" action="ex7.php">
        Número 1: <input type="text" name="num1"> <br>
        Número 2: <input type="text" name="num2"> <br>
        <input type="submit" value="Enviar">
    </form>
</body>
</html>