<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Exercici 2 pil4</title>
</head>
<body>
    <?php
        // Declaració de variables
        $num1 = $_POST["num1"];
        $num2 = $_POST["num2"];
        $num3 = $_POST["num3"];
        $num4 = $_POST["num4"];
        $num5 = $_POST["num5"];
        function suma($num1, $num2, $num3, $num4, $num5) {
            // Calculem la suma i la mostrem per pantalla
            $suma = $num1 + $num2 + $num3 + $num4 + $num5;
            return $suma;
        }
        // Cridem a la funció guardant el resultat en una variable i mostrem la variable
        $tmp = suma($num1, $num2, $num3, $num4, $num5);
        echo "Suma = $tmp";
    ?>
</body>
</html>