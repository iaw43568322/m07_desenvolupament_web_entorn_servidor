<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Exercici 7 pil4</title>
</head>
<body>
    <?php
        // Declaració de variables
        $num1 = $_POST["num1"];
        $num2 = $_POST["num2"];
        $num3 = $_POST["num3"];
        $num4 = $_POST["num4"];
        $num5 = $_POST["num5"];
        $num6 = $_POST["num6"];
        $num7 = $_POST["num7"];
        $numeros = array($num1, $num2, $num3, $num4, $num5, $num6, $num7);
        /**
         * Funció que ordena una llista de números de menor a major i els separa
         * en parells o senars
         * 
         * @param array llista de números
         * @return array dues llistes de números
         */
        function separar($numeros) {
          $numerosOrdenats = array(
                                    "parells" => array(),
                                    "senars" => array()
                                    );
          $parells = array();
          $senars = array();
            // Separem els números en parells i senars
            for ($i = 0; $i < count($numeros); $i++) {
                if ($numeros[$i] % 2 == 0) {
                    array_push($numerosOrdenats["parells"], $numeros[$i]);
                } else {
                    array_push($numerosOrdenats["senars"], $numeros[$i]);
                }
            }
            // Ordenem els arrays parells i senars de menor a major
            sort($numerosOrdenats["parells"]);
            sort($numerosOrdenats["senars"]);
            
            //var_dump($numerosOrdenats["parells"]);

            return $numerosOrdenats;
            }
        echo var_dump(separar($numeros));
    ?>
</body>
</html>