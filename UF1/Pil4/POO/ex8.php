<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Exercici 8 pil4</title>
</head>
<body>
    <?php
        class Empleado {
            // Atributs
            private $nom;
            private $sueldo;

            // Mètodes
            /**
             * Funció que dona un valor a cada atribut
             * 
             * @param String nom del treballador
             * @param int sou del treballador
             */
            public function inicializar($nom, $sueldo) {
                $this->nom = $nom;
                $this->sueldo = $sueldo;
            }
            /**
             * Funció que retorna el nom de la persona i diu si ha de pagar impostos
             * o no
             */
            public function impuestosEmpleado() {
                $impostos = false;
                // Si el sou supera els 3000 paga impostos
                if ($this->sueldo > 3000) {
                    $impostos = true;
                }
                // Retornem el missatge
                if ($impostos) {
                    echo "El treballador ". $this -> nom ." ha de pagar impostos";
                } else {
                    echo "El treballador ". $this->nom ." no ha de pagar impostos";
                }
            }
        }

        // Main
            $empl1=new Empleado();
            $empl2 = new Empleado();
            $empl1->inicializar("Ignasi Brugada", 5000);
            $empl2->inicializar("Paco", 1200);
            $empl1->impuestosEmpleado();
            $empl2->impuestosEmpleado();
    ?>
</body>
</html>