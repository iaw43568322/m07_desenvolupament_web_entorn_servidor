<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Exercici 9 POO</title>
</head>
<body>
    <?php
        class ListaHipervinculos {
            
            // Atributs
            private $hipervinculos = array("https://www.youtube.com/", "https://www.instagram.com/");

            // Métodes
            /**
             * Funció que afegeix un nou enllaç al menú
             * 
             * @param String enllaç que es vol afegir
             */
            public function cargarOpcion($enlace) {
                array_push($this -> hipervinculos, $enlace);
            }
            /**
             * Funció que visualitza el menú en una pàgina HTML
             */
            public function mostrar() {
                $cont = 1;
                $mostrar = "<ul>";
                // Bucle que recorre tot l'array
                foreach ($this -> hipervinculos as $valor) {
                    $mostrar = $mostrar. "<li style='display:inline'>
                                    <a href=". $valor .">Enlace ". $cont ."</a>
                                </li>";
                    $cont++;
                }
                $mostrar = $mostrar. "</ul>";
                
                echo $mostrar;
            }
        }

        $lista = new ListaHipervinculos();
        $lista -> mostrar();
        $lista -> cargarOpcion("https://www.netflix.com/es/");
        $lista -> mostrar();
    ?>
</body>
</html>