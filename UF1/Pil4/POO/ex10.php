<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Exercici 10 POO</title>
</head>
<body>
    <?php
        class CabeceraPagina {

            // Atributs
            private $titol;
            private $centrat;
            private $dreta;
            private $esquerra;
            private $colorFons;
            private $colorLletra;

            // Métodes
            /**
             * Funció que estableix els valors a tots els atributs
             * 
             * @param String títol
             * @param boolean si el volem centrat o no
             * @param boolean si el volem a la dreta o no
             * @param boolean si el volem a l'esquerra o no
             * @param String el color de fons que volem
             * @param String el color de lletra que volem
             */
            public function inicializar($titol, $centrat, $dreta, $esquerra,
                                         $colorFons, $colorLletra) {
                $this -> titol = $titol
                $this -> centrat = $centrat
                $this -> dreta = $dreta
                $this -> esquerra = $esquerra
                $this -> colorFons = $colorFons
                $this -> colorLletra = $colorLletra
            }
            /**
             * Funció que visualitza el menú en una pàgina HTML
             */
            public function mostrar() {
                $mostrar = "<h1 style='backgroundColor:". $this -> colorFons ."
                            ;color:". $this -> colorLletra ."'>". $this -> titol ."</h1>";
                echo $mostrar;
            }
        }

        // Main
        $cabecera = new CabeceraPagina();
        $cabecera -> inicializar("Pova de la capçalera", true, false, false, "red", "orange");
        $cabecera -> mostrar();
    ?>
</body>
</html>