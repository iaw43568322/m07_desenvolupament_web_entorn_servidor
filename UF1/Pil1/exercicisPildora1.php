<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Exercicis Pildora 1</title>
</head>
<body>
    <h1>Pildora 1: Syntaxis</h1>
    <h5>
        2. Crea una pagina en la que se almacenan dos numeros en variables y muestre
        en una tabla los valores que toman las variables y cada uno de los resultados
        de todas las operaciones aritmeticas. 
    </h5>
    <?php 
        $a = 4;
        $b = 3;
        $suma = $a + $b;
        $resta = $a - $b;
        $multiplicacion = $a * $b;
        $division = $a / $b;
        $potencia = $a ** $b;
    ?>

    <table>
        <tr>
            <th>Operacion</th>
            <th>Valor</th>
        </tr>
        <tr>
            <td>A</td>
            <td> <?php echo "$a" ?> </td>
        </tr>
        <tr>
            <td>B</td>
            <td> <?php echo "$b" ?> </td>
        </tr>
        <tr>
            <td>A + B</td>
            <td> <?php echo "$suma" ?> </td>
        </tr>
        <tr>
            <td>A - B</td>
            <td> <?php echo "$resta" ?> </td>
        </tr>
        <tr>
            <td>A * B</td>
            <td> <?php echo "$multiplicacion" ?> </td>
        </tr>
        <tr>
            <td>A / B</td>
            <td> <?php echo "$division" ?> </td>
        </tr>
        <tr>
            <td>A exp B</td>
            <td> <?php echo "$potencia" ?> </td>
        </tr>
    </table>
</body>
</html>