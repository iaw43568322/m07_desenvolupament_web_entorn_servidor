<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Exercicis Fitxers</title>
</head>
<body>
    <h1>Píldora 5: Ficheros</h1>
    <h5>
        1. Crea un formulario que puedas subir un archivo. En la pagina de destino
        ha de verse los atributos del archivo: nombre, tamaño, ubicación temporal,
        extensión…
    </h5>
    <form action="ex1.php" method="POST" enctype="multipart/form-data">
        <label>Filename</label>
        <input type="file" name="fitxer" id="fitxer1">
        <input type="submit" value="Enviar">
    </form>
    <h5>
        2. Crea un formulario que puedas subir varios archivos. En la pagina
        de destino ha de verse los atributos de los archivos.
    </h5>
    <form action="ex2.php" method="POST" enctype="multipart/form-data">
        <label>Filename</label>
        <input type="file" name="fitxers[]" multiple="">
        <input type="submit" value="Enviar">
    </form>
    <h5>
        3. Crea un programa que subamos mediante un formulario un archivo.
        En la pagina de respuesta se ha de mover el archivo a una carpeta en el 
        servidor llamada "/subidas" y mostrar todos los archivos que existen
        en esa carpeta.
    </h5>
    <form action="ex3.php" method="POST" enctype="multipart/form-data">
        <label>Filename</label>
        <input type="file" name="fitxer">
        <input type="submit" value="Enviar">
    </form>
</body>
</html>