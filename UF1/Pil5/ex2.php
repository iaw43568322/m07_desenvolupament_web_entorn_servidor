<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Exercici 2 Pil5</title>
</head>
<body>
    <?php
        // Fem un foreach per recorre tots els fitxers que s'han enviat
        foreach ($_FILES["fitxers"]["tmp_name"] as $key => $tmp_name) {
            $filename = $_FILES["fitxers"]["name"][$key];
            $filesize = $_FILES["fitxers"]["size"][$key];
            $fileExtension = pathinfo($_FILES["fitxers"]["name"][$key], PATHINFO_EXTENSION);

            echo "Nom del fitxer: ". $filename ."<br>Tamany del fitxer: ". 
            $filesize ."<br>Extensió del fitxer: ". $fileExtension ."<br><br>";
        }
    ?>
</body>
</html>