<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class productsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert(
            [
                'product'   => 'zapatillas nike',
                'stock'     => '22',
                'price'     => '120'
            ]
        );

        DB::table('products')->insert(
            [
                'product'   => 'sudadera elese',
                'stock'     => '70',
                'price'     => '88'
            ]
        );
    }
}
