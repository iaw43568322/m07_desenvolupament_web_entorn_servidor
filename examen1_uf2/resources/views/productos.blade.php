@extends('layouts/layout')
@section('title', 'Productos')
@section('content')
<div class="container">
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Producto</th>
                <th scope="col">Stock</th>
                <th scope="col">Precio</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row">1</th>
                <td>Zapatillas Nike</td>
                <td>22</td>
                <td>120€</td>
            </tr>
            <tr>
                <th scope="row">2</th>
                <td>Sudadera elese</td>
                <td>70</td>
                <td>88€</td>
            </tr>
            <tr>
                <th scope="row">3</th>
                <td>Gorra Balenciaga</td>
                <td>10</td>
                <td>300€</td>
            </tr>
        </tbody>        
    </table>
</div>
@endsection