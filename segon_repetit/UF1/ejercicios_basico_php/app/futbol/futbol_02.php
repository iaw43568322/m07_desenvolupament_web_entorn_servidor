<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
    <title>Futbol 02</title>
</head>
<body>
    <?php
        // Guardamos el numero de jugadores y de partidos
        $numJugadors = $_GET["numJugadors"];
        $numPartits = $_GET["numPartits"];
        // var_dump($numJugadors . $numPartits);
    ?>
    <div class="border m-4 p-4">
        <form action="futbol_03.php" method="post">
            <input type="hidden" name="numJugadors" value="<?=$numJugadors?>">
            <input type="hidden" name="numPartits" value="<?=$numPartits?>">
            <h3>Partidos</h3>
            <?php
                // Bucle para recorrer el numero de partidos
                for ($i = 0; $i < $numJugadors; $i++) {
            ?>
            <br>
            <p>Jugador <?=$i + 1?></p>
            <label for="nomJugador">Nombre del jugador: </label>
            <input type="text" name="nomJugador[]">
            <br><br>
            <?php
                    // Bucle para recorrer el numero de jugadores
                    for ($j = 0; $j < $numPartits; $j++) {
            ?>
            <label for="goles">Goles partido <?=$j + 1?>: </label>
            <input type="text" name="goles[<?= $i ?>][]">
            <br>
            <?php
                    }
                }
            ?>
            <br>
            <input type="submit" value="send">
        </form>
    </div>    
</body>
</html>