<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
    <title>Document</title>
</head>
<body>
    <?php
        // Guardamos los nombres de los jugadores y el numero de goles
        $numJugadors = $_POST["numJugadors"];
        $numPartits = $_POST["numPartits"];
        $jugadors = $_POST["nomJugador"];
        $goles = $_POST["goles"];
        
    ?>
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>Jugador</th>
                <?php
                    // Bucle para mostrar los partidos
                    for ($i = 0; $i < $numPartits; $i++) {
                ?>
                <th>Partido <?= $i + 1 ?></th>
                <?php
                    }
                ?>
            </tr>
            <?php
                // Bucle per mostrar tots els jugadors
                for ($i = 0; $i < count($jugadors); $i++) {
            ?>
            <tr>
                <th><?=$jugadors[$i]?></th>
                <?php
                    // Bucle per mostrar els gols de cada jugador
                    for ($j = 0; $j < $numPartits; $j++) {
                ?>
                <td><?= $goles[$i][$j] ?></td>
                <?php
                    }
                ?>
            </tr>
            <?php
                }
            ?>
        </thead>
    </table>
</body>
</html>