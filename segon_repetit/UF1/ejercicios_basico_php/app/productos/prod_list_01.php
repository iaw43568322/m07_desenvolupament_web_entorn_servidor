<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
    <title>Products</title>
</head>
<body>
    <nav class="navbar navbar-dark bg-dark">
        <p class="navbar-brand">Product's List<p>
    </nav>
    <br><br>
    <div class="border p-4 m-4">
        <form action="prod_list_02.php" method="get">
            <label for="numProducts">Numero de productos</label>
            <input type="text" name="numProducts">
            <input type="submit" value="send">
        </form>
    </div>
</body>
</html>