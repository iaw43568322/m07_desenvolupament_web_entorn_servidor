<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
    <title>Products list</title>
</head>
<body>
    <?php
        // Guardamos todos los nombres y precios de los productos pasados en un array
        $productsName = $_POST["productName"];
        $productsPrice = $_POST["productPrice"];
        echo $productsName[2];
        // var_dump($productsName . $productsPrice);
    ?>
    <table class="table">
        <thead>
            <tr>
                <td>#</td>
                <td>
                    Product Name
                </td>
                <td>
                    Price
                </td>
            </tr>
        </thead>
        <tbody>
            <?php
                // Hacemos un bucle para mostrar todos los productos
                for ($i = 0; $i < count($productsName); $i++) {
                    if ($productsName[$i] == "" or $productsPrice[$i] == "") {                  
            ?>
            <tr>
                <th>Product not inserted</th>
            </tr>
            <?php
                    } else {
            ?>
            <tr>
                <th scope="row"><?=$i + 1?></th>
                <th><?=$productsName[$i]?></th>
                <th><?=$productsPrice[$i]?></th>
            </tr>
            <?php
                    }
                }
            ?>
        </tbody>
    </table>
</body>
</html>