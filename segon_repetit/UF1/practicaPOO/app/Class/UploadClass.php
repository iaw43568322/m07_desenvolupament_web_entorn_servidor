<?php
class UploadError extends Exception {

}

class Upload {
    // Atributos
    private $pictureName;
    private $error;

    /**
     * Constructor con todos los atributos
     * 
     * $name : nombre del elemento del formulario del que gestionamos el $_FILE
     */
    public function __construct($pictureName, $pictureTmpName) {
        $this->pictureName = $pictureName;
        // Llamamos a la función upload para que guarde la foto
        $this->error = $this->upload($pictureName, $pictureTmpName);
    }

    /**
     * upload: Función que hace las operaciones necesarias para subir el archivo al servidor
     */
    public function upload($pictureName, $pictureTmpName) {
        // Comprovamos si el fichero ya existe en la carpeta fotos y en caso contrario lo subimos
        if (file_exists("./fotos/" . $pictureName)) {
            return 1;
        } else {
            move_uploaded_file($pictureTmpName, "./fotos/" . $pictureName);
            return 0;
        }
        /* try {
            if (file_exists("./fotos/" . $pictureName)) {
                throw new UploadError("El archivo $pictureName ya existe");
            }
            move_uploaded_file($pictureTmpName, "./fotos/" . $pictureName);
        } catch (UploadError $e) {
            echo "Error de subida: " . $e-getMessageError();
        } catch (Exception $e) {
            echo $e->getMessageError();
        } */
    }

    // Getters
    public function getPath() {
        return "./fotos/" . $this->pictureName;
    }

    public function getError() {
        return $this->error;
    }

    public function getMessageError() {
        // Mostramos mensajes diferentes dependiendo del error
        if ($this->error == 0) {
            return "Imagen $this->pictureName subida con exito";
        } else {
            return "ERROR: $this->pictureName is already exists";
        }
    }

}
?>