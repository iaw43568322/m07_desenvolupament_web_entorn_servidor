<?php
class Persona {
    // Atributos
    private $name;
    private $surname;
    private $address;
    private $comments;
    private $picture;

    /**
     * Constructor con todos los atributos
     */
    public function __construct($name, $surname, $address, $comments, $picture) {
        $this->name = $name;
        $this->surname = $surname;
        $this->address = $address;
        $this->comments = $comments;
        $this->picture = $picture;
    }

    // Setters
    public function setName($name){
        $this->name = $name;
    }
    public function setSurname($surname){
        $this->surname = $surname;
    }
    public function setAddress($address){
        $this->address = $address;
    }
    public function setComments($comments){
        $this->comments = $comments;
    }
    public function setPicture($picture){
        $this->picture = $picture;
    }

    // Getters
    public function getName() {
        return $this->name;
    }
    public function getSurname() {
        return $this->surname;
    }
    public function getAddress() {
        return $this->address;
    }
    public function getComments() {
        return $this->comments;
    }
    public function getPicture() {
        return $this->picture;
    }
}
?>