<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
    <title>Ficha alumno</title>
</head>
<body>
    <nav class="navbar navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Upload Person</a>
    </nav>
    <div class="border m-4 p-4">
        <form action="ficha_alumno_view.php" method="post"
            enctype="multipart/form-data">
            <h2>Upload Person</h2>
            <label for="name">Name:</label><br>
            <input type="text" name="name"><br>
            <label for="surname">Surname:</label><br>
            <input type="text" name="surname"><br>
            <label for="address">Address:</label><br>
            <input type="text" name="address"><br>
            <label for="comments">Comments:</label><br>
            <input type="text" name="comments"><br>
            <label for="picture">Picture:</label><br>
            <input type="file" name="picture">
            <br><br>
            <input class="" type="submit" value="Upload">
        </form>
    </div>
</body>
</html>