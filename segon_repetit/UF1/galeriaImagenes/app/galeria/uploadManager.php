<?php
include('Class/PictureClass.php');

$title = $_POST["title"];
$filename = $_FILES["picture"]["name"];
$tmpName = $_FILES["picture"]["tmp_name"];
// Call the PictureClass constructor
$picture = new Picture($title, $filename);
// Save the file title
// $file_uploaded;
$title_uploaded = $picture->getTitle();
// Call uploadPicture and addPictureToFile function
uploadPicture($picture);
if ($file_uploaded != null) {
    addPictureToFile($file_uploaded, $title_uploaded);
}


/*
* Función que se encarga de subir un archivo y moverlo a la carpeta /pictures
* que almacena todas las fotos.
* Return: Devuelve la ruta final del archivo.
*/
function uploadPicture($picture){
    // Save the $picture atributes
    $filename = $picture->getFilename();
    global $tmpName;
    try {
        // Check if file $picture exists in folder 'pictures'
        if (file_exists("./pictures/" . $filename)) {
            throw new UploadError("The file " . $filename . " is already exists");
        }
        // Move the file in folder pictures
        move_uploaded_file($tmpName, "./pictures/" . $filename);
        // Save the file path
        global $file_uploaded;
        $file_uploaded = "pictures/" . $filename;
        // return "./pictures/" . $filename;
        header("Location:index.php?upload=success");
    } catch (UploadError $e) {
        // return "Error de subida: " . $e->getMessage();
        header("Location:index.php?upload=error&msg=" . urlencode($e->getMessage()));
    } catch (Exception $e) {
        // return $e->getMessage();
        header("Location:index.php?upload=error&msg=" . urlencode($e->getMessage()));
    }

}

/*
* Función que se encarga de añadir al archivo fotos.txt el titulo y la ruta de la
* fotografía recien subida
* Entradas:
*       $file_uploaded: La ruta del archivo
*       $title_uploaded: El titulo del archivo
* Return: null
*/
function addPictureToFile($file_uploaded,$title_uploaded){
    // Open file fotos.txt in append mode
    $fotosFile = fopen('fotos.txt', 'a');
    // Write the new line
    fwrite($fotosFile, $title_uploaded . "###" . $file_uploaded . "\n");
    fclose($fotosFile);
    return null;
}

/*
* Clase personalizada extendida de Exception que utilizaremos para lanzar errores
* en la subida de archivos. Por ejemplo:
* throw new UploadError("Error: Please select a valid file format.");
*/
class UploadError extends Exception{}
?>