<?php
class Gallery {
    /* Atributes */
    private $filename;

    /* Constructor: Recives the path of file fotos.txt */
    public function __construct($filename) {
        // Call the loadGallery function
        $this->filename = $filename;
    }

    /**
     * Read the file fotos.txt and for each line create a Picture object,
     * then add this object in the array $_galley
     */
    public function loadGallery($filename) {
        $fotos;
        $file = fopen($filename, "r");
        /* while (fgets($file)) {
            $line = fgets($file);
            $prova = $line . "\n";
        } */
        $i = 0;
        while (($buffer = fgets($file, 4096)) !== false) {
            $fotos[] = explode("###", $buffer);
            // $i++;
        }
        return $fotos;
        // fclose($file);
        // return $prova;
        // return fgets($file, 2);
    }
}
?>