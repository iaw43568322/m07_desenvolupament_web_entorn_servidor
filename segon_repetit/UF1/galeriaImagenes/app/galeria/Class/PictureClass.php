<?php
class Picture {
    /* Atributes */
    private $title;
    private $filename;

    /* Constructor */
    public function __construct($title, $filename) {
        // Save the picture atributes
        $this->title = $title;
        $this->filename = $filename;
        
    }

    /* Getters & Setters */
    public function setTitle($title) {
        $this->title = $title;
    }
    public function getTitle() {
        return $this->title;
    }
    public function setFilename($filename) {
        $this->filename = $filename;
    }
    public function getFilename() {
        return $this->filename;
    }
}
?>