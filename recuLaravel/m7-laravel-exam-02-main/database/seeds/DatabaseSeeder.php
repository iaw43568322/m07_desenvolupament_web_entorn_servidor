<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       DB::table('movies')->insert([
           'name' => 'El señor de los anillos',
           'category' => 1,
           'description' => 'Algo de un anillo',
           'rating' => 4,
           'stock' => 7,
           'price' => 10.10,
           'image' => 'img01.jpg',
       ]);

       DB::table('movies')->insert([
        'name' => 'Ironman',
        'category' => 2,
        'description' => 'Un tio con mucha pasta',
        'rating' => 4,
        'stock' => 1,
        'price' => 5,
        'image' => 'img02.jpg',
        ]);

        DB::table('movies')->insert([
            'name' => 'Passengers',
            'category' => 3,
            'description' => 'Una nave espacial',
            'rating' => 2,
            'stock' => 3,
            'price' => 1,
            'image' => 'img03.jpg',
        ]);

        DB::table('movies')->insert([
            'name' => 'V de Vendetta',
            'category' => 2,
            'description' => 'Venganza',
            'rating' => 2,
            'stock' => 3,
            'price' => 1,
            'image' => 'img04.jpg',
        ]);
    }
}
