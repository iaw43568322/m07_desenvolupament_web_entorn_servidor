<?php

namespace App\Http\Controllers;

use App\Models\Product;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    private $product;
    private $query;

    public function __construct() {
        $this->product = new Product();
        $this->query = $this->product->query();
    }

    public function inicio(Request $request) {
        $products = $this->listProducts();
        $carrito = $this->listCarrito($request);

        return view('home')->with(['products' => $products])->with(['carrito' => $carrito]);
    }

    public function listProducts() {
        return $this->product->get();
    }

    public function addCarrito(Request $request) {
        $id = $_POST['id'];
        $prod = $this->product->findById($id)->get();
        
        $carrito = $request->session()->get('carrito', []);
        array_push($carrito, $prod[0]);
        $request->session()->put('carrito', $carrito);

        return redirect(url()->previous());
    }

    public function listCarrito(Request $request) {
        return $request->session()->get('carrito', []);
    }

    public function deleteCarrito(Request $request) {
        $request->session()->forget('carrito');
        return redirect(url()->previous());
    }

    public function comprar(Request $request) {
        return redirect(url(route('resumen')));
    }

    public function resumenCompra(Request $request) {
        return view('resumenCompra')->with(['products' => $request->session()->get('carrito')]);
    }

    public function pagar() {
        return view('pagar');
    }

    public function confirmarPago() {
        $info = [
            "name"  => $_POST['name'],
            "surname"   => $_POST['surname'],
            "email" => $_POST['email']
        ];

        return view('confirmarPago')->with(['info' => $info]);
    }

    public function finalizarPedido(Request $request) {
        $request->session()->forget('carrito');

        return redirect(url('/'));
    }
}
