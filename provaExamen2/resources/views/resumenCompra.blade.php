<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Resumen</title>
</head>
<body>
    <a href="pagar"><button class="btn btn-primary">Pagar</button></a>
    @forelse ($products as $prod)
        <div class="col col-4 d-flex flex-wrap mt-5">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="{{ $prod->image }}" alt="Card image cap">
                <div class="card-body text-center">
                    <h3 class="card-title">{{ $prod->title }}</h3>
                    <p class="card-text">{{ $prod->sinopsis }}</p>
                    <form method="POST" action="/addCarrito">
                        {{ csrf_field() }}
                        <input type="hidden" value="{{ $prod->idVideo }}" name="id">
                        <input type="submit" value="Comprar">
                    </form>
                </div>
            </div>
        </div>
    @empty
        <p>No hay productos</p>
    @endforelse
</body>
</html>