<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Pagar</title>
</head>
<body>
    <form method="POST" action="{{ route('confirmarPago') }}">
        {{ csrf_field() }}
        <label for="name">Nombre</label>
        <input type="text" name="name">
        <br><br>
        <label for="surname">Apellidos</label>
        <input type="text" name="surname">
        <br><br>
        <label for="email">Email</label>
        <input type="text" name="email">
        <br><br>
        <input type="submit" value="Pagar" class="btn btn-primary">
    </form>
</body>
</html>