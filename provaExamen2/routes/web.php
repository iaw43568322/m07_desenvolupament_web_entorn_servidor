<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('welcome');
}); */

Route::get('/', [ProductController::class, 'inicio']);

Route::post('/addCarrito', [ProductController::class, 'addCarrito']);

Route::get('/deleteCarrito', [ProductController::class, 'deleteCarrito']);

Route::get('/products/comprar', [ProductController::class, 'comprar'])->name('comprar');

Route::get('/products/resumen', [ProductController::class, 'resumenCompra'])->name('resumen');

Route::get('/products/pagar', [ProductController::class, 'pagar'])->name('pagar');

Route::post('/products/confirmar', [ProductController::class, 'confirmarPago'])->name('confirmarPago');

Route::get('/confirmarPedido', [ProductController::class, 'finalizarPedido']);
