<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class CompraRequest extends FormRequest
{
    //protected $redirectRoute = 'post.create' //ruta definida en alguno de los archivos de la carpeta routes
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'password-confirm' => 'required',
            'foto' => 'required'
        ];
    }

    public function messages()
    {
        return [
           'name.required' => 'El nombre es obligatorio',
           'email.required' => 'El email es obligatorio',
            'password.required' => 'La contraseña es obligatoria',
            'password-confirm.required' => 'Repita la contraseña',
            'foto.required' => 'La foto es obligatoria'
        ];
    }
    public function attributes()
    {
        return [
        ];
    }
    /**
     *  AJAX Response 
     */
    public function response(array $errors)
    {
        if ($this->expectsJson()) {
            return new JsonResponse($errors, 422);
        }
        return $this->redirector->to($this->getRedirectUrl())
            ->withInput($this->except($this->dontFlash))
            ->withErrors($errors, $this->errorBag);
    }
}
