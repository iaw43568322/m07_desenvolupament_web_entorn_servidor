<?php
namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;

class ProductController extends Controller
{    
   
    private $products;
    private $_filters;

    public function __construct()
    {
        /**
         * Filters (name=>value) format to show in the view
         * Write the content of the stars
         */
        $this->_filters=(object)array(
            'category'=>array('Categori1'=>'cat1','Categori2'=>'cat3','Categori3'=>'cat3'),
            'stars'=>array()
        );
    }
    /**
     * Method to list all the products
     */
    public function all()
    {   
    }

    /**
     * Method to list the products filtered by category
     */
    public function category()
    {   

    }

    /**
     * Method to list the products filtered by stars
     */
    public function stars()
    {

    }

    /**
     * Método para añadir un producto al carrito
     */
    public function addToCart(Request $request) {
        // Creamos la session
        $carrito = $request->session()->get('carrito', []);
        // Guardamos el valor del form
        array_push($carrito, (object) $request->all());
        // Ponemos el valor en la session
        $request->session()->put('carrito', $carrito);
        return redirect(url()->previous());
    }
}