var path = require("path");
var ctrlDir = "/app/app/controllers";
var express = require("express");
var router = express.Router();
var alumnosCntr = require(__dirname+'/../controllers/alumnos');

router.route("/").get(async(req, res, next) => {
    var res = await alumnosCntr.load();
    console.log(res);
    res.render("chat");
});

module.exports = router;