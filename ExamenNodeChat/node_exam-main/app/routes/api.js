var path = require("path");
var ctrlDir = "/app/app/controllers";
var express = require("express");
var router = express.Router();
var alumnosCntr = require(__dirname+'/../controllers/alumnos');

/* router.get("/", function(req, res, next) {
    res.render("chat", {title: "Chat"});
}); */

router.route("/").get(async(req, res, next) => {
    var res = await alumnosCntr.load();
    console.log(res);
});

module.exports = router;