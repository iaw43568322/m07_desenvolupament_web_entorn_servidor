var mongoose = require("mongoose");
Schema = mongoose.Schema;

// Creamos el modelo de docente
var docenteSchema = new Schema({
    nombre: {type: String},
    apellido: {type: String}
});

module.exports = mongoose.model("docente", docenteSchema);