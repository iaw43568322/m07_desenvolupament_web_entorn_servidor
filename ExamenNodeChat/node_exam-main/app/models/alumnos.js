var mongoose = require("mongoose");
Schema = mongoose.Schema;

// Creamos el modelo de alumno
var alumnoSchema = new Schema({
    nombre: {type: String},
    apellido: {type: String}
});

module.exports = mongoose.model("alumno", alumnoSchema);