var mongoose = require("mongoose");
Schema = mongoose.Schema;

// Modelo assignatura
var asignaturaSchema = new Schema ({
    nombre: {type: String},
    num_horas: {type: String},
    docente: {type: Object},
    alumnos: {type: [Object]}
});

module.exports = mongoose.model("asignatura", asignaturaSchema);